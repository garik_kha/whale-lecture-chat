angular.module('starter.controllers', [])

.controller('LoginCtrl', function($scope, $state, $ionicModal){
  
  $ionicModal.fromTemplateUrl('signup-modal.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });
  $scope.openModal = function() {
    $scope.modal.show();
  };
  $scope.closeModal = function() {
    $scope.modal.hide();
  };
  // Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });
  // Execute action on hide modal
  $scope.$on('modal.hidden', function() {
    // Execute action
  });
  // Execute action on remove modal
  $scope.$on('modal.removed', function() {
    // Execute action
  });

  $scope.googleSign = function(){
    var provider = new firebase.auth.GoogleAuthProvider();
    provider.addScope('https://www.googleapis.com/auth/plus.login');
    
    
    firebase.auth().signInWithPopup(provider).then(function(result){
      var token = result.credential.accessToken;
      var user = result.user;
      alert(user);
      //testing
    }).catch(function(error){
      alert('Something Failed');
    });
  }

  $scope.signupEmail = function(user){
    firebase.auth().createUserWithEmailAndPassword(user.email, user.password).then(function(){
      console.log('Account Created!');
      $state.go('tab.account');
    }).catch(function(error){
      console.log('Error occured while trying to make an account');
      alert(error.message);
    })
  }
  $scope.loginEmail = function(user){
   
   firebase.auth().signInWithEmailAndPassword(user.email, user.password).then(function(){
    console.log('Logged in!');
    $state.go('tab.dash');
  }).catch(function(error){
    console.log('Error occured while logging in');
    console.log(error.code);
    alert(error.message)
  });
  }
})

/*
  $scope.signIn = function(user){
    $state.go('tab.dash')

    firebase.auth().signInWithEmailAndPassword(user.email, user.password).catch(function(error){
      if(error){
        alert(error.message);
      }else{
        alert('Logged in!?');   
      }
    })
  }

  
  $scope.signIn = function(user){
    console.log('Hi there');
    firebase.auth().signInWithEmailAndPassword(user.email, user.password).catch(function(error){
      var errorCode = error.code;
      var errorMessage = error.message;

      console.log(errorCode);
      alert(errorMessage);
    });
    }

    */


.controller('WordsCtrl', function($scope, $state, Words) {
  $scope.wordList = Words.all();
  $scope.addWord = function(word){
    wordList = Words.all();
    if(wordList.indexOf(word) > -1){
      alert('That word is already in the list! Refreshed and placed it at the top.');
      $scope.wordList = Words.refreshWord(word);
    }else{
    $scope.wordList = Words.addWord(word);
  }}


})

.controller('DashCtrl', function($scope) {})

.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope, $state) {
  $scope.settings = {
    enableFriends: true
  };
  $scope.signOut = function(user){
    $state.go('tab.dash');
  }
});
